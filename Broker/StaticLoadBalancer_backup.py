from IPtoLatLong import IPtoLatLong
import sys
import random
from StateManagement import StateManagement
from PhysicalBrokerState import PhysicalLoadInfo

class StaticLoadBalancer(object):
    def __init__(self):
        pass

    def getNext(self):
        pass
        # minDelay = sys.maxsize
        #
        # broker = None
        #
        # for brokerIP in StateManagement.getInstance().brokerList:
        #
        #     if minDelay <= StateManagement.getInstance().brokerState[brokerIP]['delayTime']:
        #         broker = brokerIP
        #         minDelay = StateManagement.getInstance().brokerState[brokerIP]['delayTime']
        #
        # return broker


class RoundRobinLoadBalancer(StaticLoadBalancer):

    def getNext(self):
        nextIndex = ((StateManagement.getInstance().roundRobinIndex + 1) % len(StateManagement.getInstance().brokerList))
        StateManagement.getInstance().roundRobinIndex = nextIndex
        return StateManagement.getInstance().brokerList[nextIndex]


class RandomLoadBalancer(StaticLoadBalancer):

    def getNext(self):
        print ('length of broker List: ', len(StateManagement.getInstance().brokerList))
        if (len(StateManagement.getInstance().brokerList) > 0):
            nextIndex = random.randint(0, len(StateManagement.getInstance().brokerList)-1)
            print ('next Index: ', nextIndex)
            print('broker list : ', StateManagement.getInstance().brokerList)
            return StateManagement.getInstance().brokerList[nextIndex]

ƒ
class NearestLoadBalancer(StaticLoadBalancer):

    def getNext(self, clientIP):
        minDistance = sys.maxsize
        print('sys.maxsize: ', minDistance)

        (client_Lat, client_Long) = IPtoLatLong.getLatLong(IPtoLatLong(), ipAddress=clientIP)
        brokers = StateManagement.getInstance().brokerList

        if (not client_Lat or not client_Long):
            return brokers[0]
        nearestBroker = brokers[0]

        for (broker, port) in brokers:
            currentDistance = IPtoLatLong.getInstance().distance(
                client_Lat, client_Long, StateManagement.getInstance().brokerState[broker]['location'][0], StateManagement.getInstance().brokerState[broker]['location'][1])

            if (currentDistance < minDistance):
                minDistance=currentDistance
                nearestBroker= (broker, port)

        return nearestBroker
