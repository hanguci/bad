import sys
import configparser
from client import *
import tornado.websocket
from tornado import gen
from tornado.websocket import websocket_connect
from tornado.ioloop import IOLoop
from clientConfig.ClientSocket import SocketListener
import threading

config = configparser.ConfigParser()

config.read('clientConfig/clientconfig.ini')

user = config.get("Client", "user")
password = config.get("Client", "pass")
print('\n user: ', user, '\n pass: ', password)

client = Client(user, password)

socketThread = SocketListener()
socketThread.setDaemon(True)
socketThread.start()

client.register()
client.login()

client.subscribe('nearbyTweetChannel','Good')

client.subscribe('nearbyTweetChannel','man')

client.subscribe('nearbyTweetChannel','Dead')

client.subscribe('nearbyTweetChannel','Walking')

time.sleep(10)


client.subscribe('nearbyTweetChannel','Live')

client.subscribe('nearbyTweetChannel','aa')

client.subscribe('nearbyTweetChannel','cc')


while True:
    print("\n\n")
    print("--------Client Commands Window----------")
    print("1 --> Logout")
    print("2 --> List Channels")
    print("3 --> List Subscriptions")
    print("4 --> Subscribe")
    print("5 --> Unsubscribe")

    print(">>")
    query = input()
    if query == "1":
        client.logout()
    elif query == "2":
        client.listchannels()
    elif query == "3":
        client.listsubscriptions()
    elif query == "4":
        channel = input()
        params = input().split()
        if not (channel):
            print('Invalid Channel Name!')
        print("Subscription ID", client.subscribe(channel, params))
        print("channelName ", channel)
        print("Para ", params)

    elif query == "5":
        channel = input()
        params = input().split()
        client.unsubcribe(channel, params)