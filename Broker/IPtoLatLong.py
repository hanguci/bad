import maxminddb
from math import radians, cos, sin, asin, sqrt
import geoip2.database
import sys
import math


class IPtoLatLong(object):
    instance = None

    @classmethod
    def getInstance(cls):
        if cls.instance is None:
            cls.instance = IPtoLatLong()
        return cls.instance

    def __init__(self):
        try:
            self.reader = geoip2.database.Reader('GeoLite2-City.mmdb')
        except:
            print("Maxmind IP to Lat Long database not present in path.")


    def getLatLong(self, ipAddress):
        try:
            response = self.reader.city(ipAddress)
            latitude = response.location.latitude
            longitude= response.location.longitude
            return (latitude,longitude)

        except KeyError as e:
            print(e)
            return None

    def distance(start_lat, start_long, end_lat, end_long):

        start_lat = math.radians(start_lat)
        start_long = math.radians(start_long)
        end_lat = math.radians(end_lat)
        end_long = math.radians(end_long)

        d_lat = math.fabs(start_lat - end_lat)
        d_long = math.fabs(start_long - end_long)

        EARTH_R = 6372.8

        y = ((math.sin(start_lat)*math.sin(end_lat)) + (math.cos(start_lat)*math.cos(end_lat)*math.cos(d_long)))

        x = math.sqrt((math.cos(end_lat)*math.sin(d_long))**2 + ( (math.cos(start_lat)*math.sin(end_lat)) - (math.sin(start_lat)*math.cos(end_lat)*math.cos(d_long)))**2)

        c = math.atan(x/y)

        print('distance: ', EARTH_R * c)
        return EARTH_R * c