import sys
import configparser
from client import *
import tornado.websocket
from tornado import gen
from tornado.websocket import websocket_connect
from tornado.ioloop import IOLoop
import threading

class SocketListener(Thread):

    def __init__(self):
        super().__init__()
        self.ioloop = IOLoop.instance()

    def run(self):
        self.ioloop.start()


    def stop(self):
        self.ioloop.stop()


if __name__ == "__main__":

    print('Run default Client!')
    config = configparser.ConfigParser()
    config.read('clientConfig/clientconfig.ini')
    user = config.get("Client", "user")
    password = config.get("Client", "pass")
    print('\n user: ', user, '\n pass: ', password)
    client = Client(user, password)
    socketThread = SocketListener()
    socketThread.setDaemon(True)
    socketThread.start()

while True:
    print("\n\n")
    print("--------Client Commands Window----------")
    print("0 --> Register")
    print("1 --> Login")
    print("2 --> Logout")
    print("3 --> List Channels")
    print("4 --> List Subscriptions")
    print("5 --> Subscribe")
    print("6 --> Unsubscribe")

    print(">>")
    query = input()
    if query == "0":
        client.register()

    if query == "1":
        client.login()

    elif query == "2":
        client.logout()

    elif query == "3":
        client.listchannels()

    elif query == "4":
        client.listsubscriptions()

    elif query == "5":
        channel = input()
        params = input()
        client.subscribe(channel, params)

    elif query == "6":
        channel = input()
        params = input()
        client.unsubscribe(channel, params)

