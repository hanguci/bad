# from IPtoLatLong import IPtoLatLong
import sys
import random
from StateManagement import StateManager
from PhysicalBrokerState import PhysicalLoadInfo

class StaticLoadBalancer(object):
    def __init__(self):
        pass

    def getNext(self):
        pass
        # minDelay = sys.maxsize
        #
        # broker = None
        #
        # for brokerIP in StateManager.getInstance().brokerList:
        #
        #     if minDelay <= StateManager.getInstance().brokerState[brokerIP]['delayTime']:
        #         broker = brokerIP
        #         minDelay = StateManager.getInstance().brokerState[brokerIP]['delayTime']
        #
        # return broker


class RoundRobinLoadBalancer(StaticLoadBalancer):

    def getNext(self):
        nextIndex = ((StateManager.getInstance().roundRobinIndex + 1) % len(StateManager.getInstance().brokerList))
        StateManager.getInstance().roundRobinIndex = nextIndex
        return StateManager.getInstance().brokerList[nextIndex]


class RandomLoadBalancer(StaticLoadBalancer):

    def getNext(self):
        # print ('length of broker List: ', len(StateManager.getInstance().brokerList))
        if (len(StateManager.getInstance().brokerList) > 0):
            nextIndex = random.randint(0, len(StateManager.getInstance().brokerList)-1)
            # print ('next Index: ', nextIndex)
            print('broker list : ', StateManager.getInstance().brokerList)
            return StateManager.getInstance().brokerList[nextIndex]

#
# class NearestLoadBalancer(StaticLoadBalancer):
#
#     def getNext(self, clientIP):
#         minDistance = sys.maxsize
#         print('sys.maxsize: ', minDistance)
#
#         (client_Lat, client_Long) = IPtoLatLong.getLatLong(IPtoLatLong(), ipAddress=clientIP)
#         brokers = StateManager.getInstance().brokerList
#
#         if (not client_Lat or not client_Long):
#             return brokers[0]
#         nearestBroker = brokers[0]
#
#         for (broker, port) in brokers:
#             currentDistance = IPtoLatLong.getInstance().distance(
#                 client_Lat, client_Long, StateManager.getInstance().brokerState[broker]['location'][0], StateManager.getInstance().brokerState[broker]['location'][1])
#
#             if (currentDistance < minDistance):
#                 minDistance=currentDistance
#                 nearestBroker= (broker, port)
#
#         return nearestBroker
