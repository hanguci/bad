import sys
from client import Client

if __name__ == '__main__':

    client = Client(sys.argv[1], sys.argv[2])
    client.register()

    if (client.login()):
        client.subscribe('WeatherTweetChannel', 'earthquake')  # 50
        # client.subscribe('WeatherTweetChannel', 'tornado')  # 4
        # client.subscribe('WeatherTweetChannel', 'flood')  # 4
        # client.subscribe('WeatherTweetChannel', 'volcano')  # 8
        # client.subscribe('WeatherTweetChannel', 'windy')  # 8
        # client.subscribe('WeatherTweetChannel', 'sunny')  # 10
        # client.subscribe('WeatherTweetChannel', 'foggy')  # 5
        # client.subscribe('WeatherTweetChannel', 'cloudy')  # 4
        # client.subscribe('WeatherTweetChannel', 'hurricane')  # 4
        # client.subscribe('WeatherTweetChannel', 'moonlight')  # 7
        # client.subscribe('WeatherTweetChannel', 'sunrise')  # 27
        # client.subscribe('WeatherTweetChannel', 'sunset')  # 20
        # client.subscribe('WeatherTweetChannel', 'the sun')  # 43
        # client.subscribe('WeatherTweetChannel', 'the moon')  # 8
        # client.subscribe('WeatherTweetChannel', 'very hot')  # 1
        # client.subscribe('WeatherTweetChannel', 'breezy') #1
        # client.subscribe('WeatherTweetChannel', 'freezing') #11
        # client.subscribe('WeatherTweetChannel', 'snowing') #1
        # client.subscribe('WeatherTweetChannel', 'blue sky') #1
        # client.subscribe('WeatherTweetChannel', 'sun shines') #1
        # client.subscribe('WeatherTweetChannel', 'raining') #11

        client.ioloop.start()

