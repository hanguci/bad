import tornado.websocket
from tornado import gen
from tornado.websocket import websocket_connect
from tornado.ioloop import IOLoop
import threading
import sys
from threading import Thread

class SocketListener(Thread):

    def __init__(self):
        super().__init__()
        self.ioloop = IOLoop.instance()

    def run(self):
        self.ioloop.start()


    def stop(self):
        self.ioloop.stop()

