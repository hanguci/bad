import requests
import json
import time
import threading
import pika
import sys
import random
import logging
from threading import Thread
import socket
import hashlib

import tornado.web
import tornado.websocket
from tornado.websocket import websocket_connect
from tornado.ioloop import IOLoop
from tornado import gen



log = logging.getLogger(__name__)


class Client:

    def __init__(self, userName, password ):

        self.userName = userName

        self.password = password
        self.userId = hashlib.sha224('{}'.format(self.userName).encode()).hexdigest()

        self.clientIP = self.get_ip_address()
        self.email = '@'
        self.brokerUrl = ''
        self.brokerName = ''
        self.brokerPort = ''

        self.ControllerServerUrl = 'http://localhost:5000'
        self.subscriptions = {}

        self.ws = None

        self.socketUrl = ''

        self.ioloop = IOLoop.instance()

    def setBroker(self, brokerServer, brokerPort):
        self.brokerUrl = 'http://{}:{}'.format(brokerServer, brokerPort)
        self.socketUrl = 'ws://{}:{}'.format(brokerServer, brokerPort) + '/websocketlistener'
        self.brokerName = brokerServer
        self.brokerPort = brokerPort

        print(self.brokerUrl)

    def get_ip_address(self):
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(('8.8.8.8', 80))
        mylocaladdr = str(s.getsockname()[0])
        return mylocaladdr

    def connectToBCS(self):

        headers = {'ClientIP':self.clientIP}
        post_data = {'userName': self.userName, 'userId': self.userId,
                     'password': self.password}

        r = requests.post(self.ControllerServerUrl + '/getBroker', data=json.dumps(post_data), headers = headers)
        if r.status_code == 200:
            response = r.json()
            if response:
                self.setBroker(response['brokerServer'], response['brokerPort'])
                log.info('Connected to BCS!')
                return True

    def register(self):
        post_data = {'userName': self.userName, 'userId': self.userId,
                     'password': self.password, 'email': self.email}

        r = requests.post(self.ControllerServerUrl + '/registeruser', data=json.dumps(post_data))
        if r.status_code == 200:
            response = r.json()
            if response:
                if response['status'] == 'success':
                    print('User `%s` registered.' % self.userName)
                    log.info('User `%s` registered.' % self.userName)
                else:
                    log.error('Registration failed for user `%s`' %self.userName)
                    log.error(response['error'])
        else:
            log.debug(r)

    def login(self):

        self.connectToBCS()

        post_data = {'userName': self.userName, 'userId': self.userId,
                     'password': self.password}

        print(self.brokerUrl + '/login')
        r = requests.post(self.brokerUrl + '/login', data=json.dumps(post_data))

        if r.status_code == 200:
            response = r.json()
            if response:
                if response['status'] == 'success':
                    print('Login successful Ok')
                    log.info('Login successful')
                    yield self.connect()
                    return True

                else:
                    log.error('Login failed %s' %response)
                    print('Login failed %s', response)
                    return False
        else:
            log.debug(r)

            return False

    def subscribe(self, channelName, parameters):
        if (channelName is None ):
            log.info('subscribe failed: empty channelname!')
            return
        if parameters is None:
            parameters = []

        post_data = {
                     'userId': self.userId,
                     'channelName': channelName,
                     'parameters': parameters}

        r = requests.post(self.brokerUrl + '/subscribe', data=json.dumps(post_data))
        print('r.status_code: ', r.status_code)
        print(post_data)
        if r.status_code == 200:
            response = r.json()
            if response:
                if response['status'] != 'success':
                    log.info('Error:' +response['error'])
                    print('Error:' + response['error'])
                    return False
                else:
                    subscriptionId = response['userSubscriptionId']
                    timestamp = response['timestamp']

                    if subscriptionId not in self.subscriptions:
                        self.subscriptions[subscriptionId] = {}

                    self.subscriptions[subscriptionId]['channelName'] = channelName
                    self.subscriptions[subscriptionId]['parameters'] = parameters
                    self.subscriptions[subscriptionId]['timestamp'] = timestamp

                    log.info('Subscription successful')
                    return subscriptionId
        else:
            log.debug(r)
            return None


    def unsubscribe(self, channelName, parameters):

        log.info('unsubscribe ')
        post_data = {
            'userId': self.userId,
            'channelName': channelName,
            'parameters': parameters
        }
        r = requests.post(self.brokerUrl + "/unsubscribe", data=json.dumps(post_data))
        if r.status_code == 200:
            response = r.json()
            if response['status'] == 'success':
                log.info('unsubscribe from channel `%s` with parameters `%s` succeeded.' % (channelName, parameters))
                return
            log.debug(r)
        else:
            print('unsubscribe from channel `%s` with parameters `%s` failed.' % (channelName, parameters))


    def logout(self):

        self.close()
        if self.userName is None:
            return
        post_data = {
            'userId': self.userId
        }
        r = requests.post(self.brokerUrl + '/logout', data=json.dumps(post_data))
        if r.status_code == 200:
            response = r.json()
            if response:
                if response['status'] == 'success':
                    print('User %s is logged out.' %self.userName)
                else:
                    log.error('logout for user %s failed due to %s' %(self.userName))
        else:
            log.error('Logout failed for %s' %self.userName)

    def listchannels(self):
        post_data = {'userId': self.userId}
        r = requests.post(self.brokerUrl + "/listchannels", data=json.dumps(post_data))
        if r.status_code == 200:
            response = r.json()
            print(response)
        else:
            log.debug(r)

    def listsubscriptions(self):
        post_data = {'userId': self.userId}
        r = requests.post(self.brokerUrl + "/listsubscriptions", data=json.dumps(post_data))
        if r.status_code == 200:
            response = r.json()
            print(response)
            subscriptions = []
            for item in response['subscriptions']:
                subscriptions.append(item['userSubscriptionId'])
            return subscriptions
        else:
            log.debug(r)

    @gen.coroutine
    def connect(self):
        print ("trying to connect")
        print(self.socketUrl)
        try:
            self.ws = yield websocket_connect(self.socketUrl)
        except Exception as e:
            print ("connection error")
        else:
            print("connected")
            self.run()

    @gen.coroutine
    def run(self):
        post_data = {'userName': self.userName, 'userId': self.userId}
        self.ws.write_message(json.dumps(post_data))
        while True:
            try:
                msg = yield self.ws.read_message()
            except Exception as e:
                log.debug(e)
            if msg is None:
                break
            print('Message received: ', msg)

    @gen.coroutine
    def close(self):
        try:
            self.ws.close()
            self.ws = None
        except Exception as e:
            print ("close connection error")
            log.debug(e)
