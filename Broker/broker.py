from brokerobjects import *
from asterixapi import *
import socket
import sys
from threading import Lock
import tornado.web
import tornado.httpclient
import tornado.websocket
import requests
import numpy as np
mutex = Lock()
live_web_sockets = set()

from DynamicLoadBalancer import DynamicLoadBalancer

from datetime import datetime, timedelta

DATAVERSE = 'pubsubtest'
SIZE = 25
CHANNEL_FREQUENCY = {'EmergencyTweetChannel':30000, 'WeatherTweetChannel':3000}

class Broker:
    def __init__(self, brokerName, brokerPort):

        self.brokerName = brokerName
        self.brokerIPAddr = self.get_ip_address()
        self.brokerPort = brokerPort
        self.clientSockets = {}
        self.ControllerServerUrl = 'http://localhost:5000'
        self.asterix = AsterixQueryManager.getInstance()
        self.registerBrokerWithBCS()
        self.channelSubscriptionTable = {}
        self.userSubscriptionTable = {}
        self.subscriptionMapping = {}
        self.sessions = {}
        # self.processingTime = {}
        self.pullData = {}
        self.pushData = {}
        self.queryTime = {}
        self.delayTime = {}
        self.deliveryTime = {}
        self.count = {}
        self.connections = set()

        # self.text_file = open('output.txt', 'w+', 1)

    def get_ip_address(self):
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(('8.8.8.8', 80))
        mylocaladdr = str(s.getsockname()[0])
        return mylocaladdr

    @tornado.gen.coroutine
    def registerBrokerWithBCS(self):
        post_request = {
                "brokerName": self.brokerName,
                "brokerIP": str(self.brokerIPAddr),
                "brokerPort": self.brokerPort
        }
        log.info(post_request)
        try:
            r = requests.post(self.ControllerServerUrl + "/registerbroker", json=post_request)
            if r and r.status_code == 200:
                pass
            else:
                log.debug('Broker registration with BCS failed !')
                exit(0)
        except Exception as e:
            log.debug('Broker registration with BCS failed for reason %s' %(e))
            exit(0)

    @tornado.gen.coroutine
    def register(self, userName, password):
        users = yield User.load(userName=userName)
        if users and len(users) > 0:
            user = users[0]
            return {
                'status': 'failed',
                'error': 'This user already exists!',
                'userId': user.userId}
        else:
            userId = hashlib.sha224('{}@{}'.format(userName, DATAVERSE).encode()).hexdigest()
            password = hashlib.sha224(password.encode()).hexdigest()
            user = User(userId, userId, userName, password)
            yield user.save()
            return {'status': 'success', 'userId': userId}

    @tornado.gen.coroutine
    def registerApplication(self, appName, appDataverse, adminUser, adminPassword, email, dropExisting=0, setupAQL=None):
        # create BrokerMetadata dataverse
        yield Application.setupApplicationEnviroment(self.asterix)
        # Check if there is already an app exist with the same name, currently ignored.
        if dropExisting == 0:
            applications = yield Application.load(dataverseName = appDataverse, appName=appName)
            if applications and len(applications) > 0:
                return {
                    'status': 'failed',
                    'error': 'Application already exists!'}
        command = 'drop dataverse {} if exists; create dataverse {};'.format(appDataverse, appDataverse)
        print(command)
        status, response = yield self.asterix.executeSQLPP(None, command)
        if status == 200:
            # Save new App into Application Dataset
            apiKey = str(hashlib.sha224((appDataverse+ appName + str(datetime.now())).encode()).hexdigest())
            adminPassword = str(hashlib.sha224((appName + adminUser + adminPassword).encode()).hexdigest())
            app = Application(Application.dataverseName, appName, appName, appDataverse, adminUser, adminPassword, email, apiKey)
            yield app.save()

            # create app datasets
            result = yield self.setupApp(appDataverse, appName)
            print("setupApp result: ", result)
            if result['status'] == 'failed':

                return result
            # Setting up application dataverse, creating broker datasets
            if setupAQL:
                response = yield self.updateApplication(appDataverse = appDataverse, appName=appName, apiKey=apiKey, setupAQL=setupAQL)

                file = open("publishers", "r")
                dataPublished = file.read()
                # print(dataPublished)
                file.close()
                if (self.populateData(DATAVERSE, dataPublished)):
                    return response
            else:
                return {
                    'status': 'success',
                    'appDataverse': appDataverse,
                    'appName': appName,
                    'apiKey': apiKey}
        else:
            return {
                'status': 'failed',
                'error': response}

    @tornado.gen.coroutine
    def setupApp(self, appDataverse, appName):
        commands = ''
        #create User type and dataset, channelSubscription type and dataset, UserSubscription type and dataset
        for cls in [User, ChannelSubscription, UserSubscription]:
            commands += cls.getCreateStatement()
        status, response = yield self.asterix.executeSQLPP(appDataverse, commands)
        if status == 200:
            return {'status': 'success'}
        else:
            return {'status': 'failed', 'error': response}

    @tornado.gen.coroutine
    def checkApplication(self, dataverseName, appName, apiKey):
        applications = yield Application.load(dataverseName=Application.dataverseName, appName=appName)
        if not applications or len(applications) == 0:
            log.error('No application `%s` exists' % appName)
            return {
                'status': 'failed',
                'error': 'No application exists'}
        elif applications[0].apiKey != apiKey:
            log.error('API key does not match')
            return {
                'status': 'failed',
                'error': 'Application APIKey does not match'}
        return {'status': 'success'}, applications[0]

    @tornado.gen.coroutine
    def populateData(self, appDataverse, publisher):
        if publisher:
            status, response = yield self.asterix.executeSQLPP(appDataverse, publisher)
        if status == 200:
            log.info('Publishing Data Success!')
            return True
        else: return False

    @tornado.gen.coroutine
    def updateApplication(self, appDataverse, appName, apiKey, setupAQL=None):
        check, app = yield self.checkApplication(appDataverse, appName, apiKey)
        if check['status'] == 'failed':
            return check
        if setupAQL is None or len(setupAQL) == 0:
            return {
                'status': 'failed',
                'error': 'No setup AQL is provided.'}
        if 'use dataverse' in setupAQL or 'create dataverse' in setupAQL:
            return {
                'status': 'failed',
                'error': 'The AQL command MUST not contain `use dataverse` or `create dataverse` commands'}
        status, response = yield self.asterix.executeSQLPP(appDataverse, setupAQL)
        log.debug(response)

        if status == 200:
            log.info('Setup for app %s is successful' %appName)
            return {
                'status': 'success',
                'appName': appName,
                'apiKey': apiKey,
            }
        else:
            log.info('Setup for app `%s` is failed' %appName)
            return {
                'status': 'failed',
                'appName': appName,
                'error': 'Setup failed, possible reason %s' % response
            }

    @tornado.gen.coroutine
    def updateState(self, userId = None, subId = None, newUser = None, newSub = None, delayTime = None):
        post_request = {
                "brokerName": self.brokerName,
                "brokerIP": self.brokerIPAddr,
                "userId": userId,
                "subId": subId,
                "newUser": newUser,
                "newSub": newSub,
                "delayTime": delayTime}
        # log.info(post_request)
        try:
            r = requests.post(self.ControllerServerUrl + "/brokerUpdate", json=post_request)
            if r and r.status_code == 200:
                pass
        except Exception as e:
            log.debug('Broker State Update with BCS:  %s' %(e))

    @tornado.gen.coroutine
    def login(self, userName, password):
        print('new User ')
        users = yield User.load(dataverseName=DATAVERSE, userName=userName)
        if users and len(users) > 0:
            user = users[0]
            password = hashlib.sha224(password.encode()).hexdigest()
            # Login success
            if password == user.password:
                userId = user.userId
                # Create a new session for this user
                user.lastLoginTime = str(datetime.now())
                # print('update user login time')
                user.save()
                self.sessions[userId] = Connection(DATAVERSE, userId, datetime.now())

                yield self.loadSubscriptionsForUser(userId = userId)

                return {'status': 'success', 'userName': userName, 'userId': userId}
            else:
                return {'status': 'failed', 'error': 'Password does not match!'}
        else:
            return {'status': 'failed', 'error': 'No user exists %s!' % userName}

    @tornado.gen.coroutine
    def checkExistingChannelSubscription(self, channelName, parameters):

        channelSubscriptions = yield ChannelSubscription.load(DATAVERSE, channelName=channelName,
                                                              brokerName=self.brokerName, parameters=parameters)
        print(channelSubscriptions)
        print(channelName)
        print(parameters)
        if channelSubscriptions and len(channelSubscriptions) > 0:
            return channelSubscriptions[0]
        else:
            print('check existing sub return False ')
            return False

    @tornado.gen.coroutine
    def subscribe(self, userId, channelName, parameters):
        #
        # self.text_file.write('new subscription from user ')
        # self.text_file.write(userId)
        #
        # self.text_file.write(' at Channel ')
        #
        # self.text_file.write(channelName)
        #
        # self.text_file.write(' with parameters ')
        #
        # self.text_file.write(parameters)
        #
        # self.text_file.write('\n')
        aql_stmt = 'SELECT value c FROM Channel c ' \
                   'WHERE DataverseName = \"%s\" and ChannelName = \"%s\" ' %(DATAVERSE, channelName)
        status, response = yield self.asterix.executeQuery('Metadata', aql_stmt)
        if status == 200:
            if not response:
                log.error('channelName not exist!')
                return None
        if channelName not in self.channelSubscriptionTable:
            self.channelSubscriptionTable[channelName] = {}
        # Convert parameters (a list) to a string
        parameters = json.dumps(parameters).replace('[','').replace(']','')
        parametersHash = str(hashlib.sha224((str(parameters)).encode()).hexdigest())
        subId = channelName + '::' + parametersHash
        # print(subId)
        # print(self.channelSubscriptionTable[channelName])
        if subId not in self.channelSubscriptionTable[channelName]:
            try:
                log.info('broker create new subscription to channel %s with params %s' % (channelName, parameters))
                channelSubscription = yield self.createChannelSubscription(channelName, parameters)
                self.channelSubscriptionTable[channelName][subId] = channelSubscription
                # self.channelSubscriptionTable[channelName][subId] = {}
            except BADException as badex:
                log.error(badex)
                return {'status': 'failed', 'error': str(badex)}

        # # Check whether this broker already made the subscription to this channel with this parameters.
        # try:
        #     ch = yield self.checkExistingChannelSubscription(channelName=channelName,parameters=parameters)
        #     print(ch)
        #     if ch:
        #         self.channelSubscriptionTable[channelName][subId] = ch
        #     else:
        #         log.info('broker create new subscription to channel %s with params %s' % (channelName, parameters))
        #         channelSubscription = yield self.createChannelSubscription(channelName, parameters)
        #         self.channelSubscriptionTable[channelName][subId] = channelSubscription
        #
        # except BADException as badex:
        #     log.error(badex)
        #     return {'status': 'failed', 'error': str(badex)}

        # print(self.channelSubscriptionTable)

        currentDateTime = yield self.getCurrentDateTime()
        userSubscription = yield self.createUserSubscription(userId, channelName, parameters, subId, currentDateTime)

        if channelName not in self.userSubscriptionTable:
            self.userSubscriptionTable[channelName] = {}
        if subId not in self.userSubscriptionTable[channelName]:
            self.userSubscriptionTable[channelName][subId] = {}
        self.userSubscriptionTable[channelName][subId][userId] = userSubscription

        if userSubscription:
            # tornado.ioloop.IOLoop.current().add_callback(self.updateState, userId=userId, subId = subId,
            #                                              newSub=1)
            print('subscribe success!')
            return {'status': 'success', 'userSubscriptionId': userSubscription.userSubscriptionId, 'timestamp': currentDateTime}
        else:
            print('subscribe fail!')
            return {'status': 'failed', 'error': 'User subscription creation failed!!'}

    @tornado.gen.coroutine
    def loadSubscriptionsForUser(self,userId):

        subs = yield UserSubscription.load(DATAVERSE, userId=userId)

        if subs is None or len(subs) == 0:
            return
        for sub in subs:
            channelName = sub.channelName
            subId = sub.subscriptionId
            parameters = sub.parameters
            # print('parameters: ' , parameters)

            if channelName not in self.channelSubscriptionTable:
                self.channelSubscriptionTable[channelName] = {}

            if(subId) not in self.channelSubscriptionTable[channelName]:
                channelSubscription = yield self.createChannelSubscription(channelName, parameters)
                self.channelSubscriptionTable[channelName][subId] = channelSubscription
            #
            # # Check whether the channel subscription is in the table
            # try:
            #     ch = self.channelSubscriptionTable[channelName][subId]
            # except KeyError as kerr:
            #     # check if the broker already made this channel subscription or create new channel subscription
            #     channelSubs = yield ChannelSubscription.load(DATAVERSE, channelName=channelName, brokerName = self.brokerName, subscriptionId = subId)
            #     if not channelSubs or len(channelSubs) == 0:
            #         log.error('The broker does not has this channel subscription yet!!! Create one!')
            #         channelSubscription = yield self.createChannelSubscription(channelName, parameters)
            #     else:
            #         channelSubscription = channelSubs[0]
            #
            #     self.channelSubscriptionTable[channelName][subId] = channelSubscription

            # Populate subscription table
            if channelName not in self.userSubscriptionTable:
                self.userSubscriptionTable[channelName] = {}
            if subId not in self.userSubscriptionTable[channelName]:
                self.userSubscriptionTable[channelName][subId] = {}
            self.userSubscriptionTable[channelName][subId][userId] = sub

    @tornado.gen.coroutine
    def unsubscribe(self, userId, channelName, parameters):
        parameters = json.dumps(parameters).replace('[','').replace(']','')

        parametersHash = str(hashlib.sha224((str(parameters)).encode()).hexdigest())
        subId = channelName + '::' + parametersHash

        userSubscription = self.userSubscriptionTable[channelName][subId][userId]

        if self.userSubscriptionTable[channelName][subId][userId]:
            del self.userSubscriptionTable[channelName][subId][userId]
        yield userSubscription.delete()

        # newSub = 0 mean unsubscribe
        # tornado.ioloop.IOLoop.current().add_callback(self.updateState, userId=userId,
        #                                              subId=subId,
        #                                              newSub= 0)
        # Delete Channel Subscription if no active user subscription for it
        if not self.userSubscriptionTable[channelName][subId]:
            channelSub = self.channelSubscriptionTable[channelName][subId]
            self.deleteChannelSubscription(channelName, channelSub.channelSubscriptionId)
            del self.userSubscriptionTable[channelName][subId]
            del self.channelSubscriptionTable[channelName][subId]
        log.info('User %s unsubscribed from %s' % (userId, channelName))
        return {'status': 'success'}

    @tornado.gen.coroutine
    def listsubscriptions(self, userId):

        userSubscriptions = yield UserSubscription.load(userId=userId)
        # print(userSubscriptions)

        return {'status': 'success', 'subscriptions': userSubscriptions if userSubscriptions else []}

    @tornado.gen.coroutine
    def listchannels(self, userId):

        aql_stmt = 'SELECT c.ChannelName as ChannelName FROM Channel c ' \
                   'WHERE DataverseName = \"%s\"' %(DATAVERSE)

        status, response = yield self.asterix.executeQuery('Metadata', aql_stmt)

        if status == 200:
            response = response.replace('\n', '')
            channels = json.loads(response)
            return {'status': 'success', 'channels': channels}
        else:
            return {'status': 'failed', 'error': response}

    @tornado.gen.coroutine
    def getChannelInfo(self, channelName):
        aql_stmt = 'SELECT value c FROM Channel c ' \
                   'WHERE DataverseName = \"%s\" and ChannelName = \"%s\"' % (DATAVERSE, channelName)

        status, response = yield self.asterix.executeQuery('Metadata', aql_stmt)
        if status == 200:
            response = response.replace('\n', '')
            channels = json.loads(response, encoding='utf-8')
            return {'status': 'success', 'channels': channels}
        else:
            return {'status': 'failed', 'error': response}

    @tornado.gen.coroutine
    def deleteChannelSubscription(self, channelName, channelSubscriptionId):

        if (channelSubscriptionId) in self.subscriptionMapping:
            del self.subscriptionMapping[channelSubscriptionId]
        # Check existing channel subscription
        statement = 'SELECT * from {0}Subscriptions Where DataverseName = \"{1}\" and BrokerName = \"{2}\" and subscriptionId  = \"{3}\"'.format(channelName, DATAVERSE, self.brokerName, channelSubscriptionId)
        status_code, response = yield self.asterix.executeSQLPP(DATAVERSE, statement)
        if status_code != 200:
            raise BADException(response)
            return
        if status_code == 200:
            if not(response):
                log.debug(response)
                return
            else:
                statement = 'unsubscribe "{0}"'.format(channelSubscriptionId) + ' from {0}'.format(channelName)
                print('statement: ', statement)
                status_code, response = yield self.asterix.executeSQLPP(DATAVERSE, statement)
                if status_code != 200:
                    log.debug(statement)
                    raise BADException(response)
                channelSubs = yield ChannelSubscription.load(dataverseName = DATAVERSE, channelSubscriptionId = channelSubscriptionId)
                if(channelSubs):
                    channelSubs[0].delete()


    @tornado.gen.coroutine
    def createChannelSubscription(self, channelName, parameters):

        result = yield self.getChannelInfo(channelName)
        if result['status'] == 'failed':
            raise BADException('Retrieving channel info failed')
        channels = result['channels']
        if len(channels) == 0:
            raise BADException('No channel exists with name %s!' % channelName)

        mutex = Lock()
        mutex.acquire()
        # Check broker
        statement = 'SELECT BrokerName from Broker Where DataverseName = \"%s\" and BrokerName = \"%s\"' % (DATAVERSE, self.brokerName)
        status_code, response = yield self.asterix.executeSQLPP('Metadata', statement)
        if status_code != 200:
            raise BADException(response)

        brokers = json.loads(response)
        if len(brokers) == 0:
            log.info('Broker `%s` is not registered. Registering..' % self.brokerName)
            command = 'create broker {} at "http://{}:{}/notifybroker"'.format(self.brokerName, self.brokerIPAddr, self.brokerPort)
            status_code, response = yield self.asterix.executeSQLPP(DATAVERSE, command)
            if status_code != 200:
                raise BADException(response)

        mutex.release()

        statement = 'subscribe to ' + channelName + '(' + parameters + ') on ' + self.brokerName + ';'
        print('statement: ', statement)
        status_code, response = yield self.asterix.executeSQLPP(DATAVERSE, statement)
        if status_code != 200:
            raise BADException(response)

        subIds = json.loads(response)

        if len(subIds) > 0:
            channelSubscriptionId = subIds[0]
            parametersHash = str(hashlib.sha224((str(parameters)).encode()).hexdigest())
            subscriptionId = channelName + '::' + parametersHash

            self.subscriptionMapping[channelSubscriptionId] = subscriptionId
            uniqueId = DATAVERSE + '::' + channelName + '::' + parametersHash
            channelSubscription = ChannelSubscription(DATAVERSE, uniqueId , channelSubscriptionId, channelName, self.brokerName, parameters, subscriptionId)
            yield channelSubscription.save()
            # print('save channel sub with parameter: ', parameters)
            return channelSubscription
        else:
            raise BADException('Subscribe to Asterix failed!')

    def makeUserSubscriptionId(self, channelName, parameters, userId):
        parameterHash = str(hashlib.sha224((str(parameters)).encode()).hexdigest())
        sid = userId + '::' + channelName + '::' + parameterHash
        return str(hashlib.sha224(sid.encode()).hexdigest())

    @tornado.gen.coroutine
    def createUserSubscription(self, userId, channelName, parameters, subscriptionId, timestamp):

        resultsDataset = channelName + 'Results'
        userSubscriptionId = self.makeUserSubscriptionId( channelName, parameters, userId)
        userSubscription = UserSubscription(DATAVERSE, userSubscriptionId, userSubscriptionId, userId,
                                    subscriptionId, channelName, parameters, timestamp, resultsDataset)
        # print(userSubscription)
        yield userSubscription.save()

        if channelName not in self.userSubscriptionTable:

            self.userSubscriptionTable[channelName] = {}

        if subscriptionId not in self.userSubscriptionTable[channelName]:
            self.userSubscriptionTable[channelName][subscriptionId] = {}

        self.userSubscriptionTable[channelName][subscriptionId][userId] = userSubscription
        return userSubscription

    @tornado.gen.coroutine
    def getCurrentDateTime(self):
        status, response = yield self.asterix.executeQuery(None, 'current_datetime()')
        if status != 200:
            return None

        log.debug(response)
        currentDateTime = json.loads(response)[0]
        log.debug('Current time at server ' + currentDateTime)
        return currentDateTime

    @tornado.gen.coroutine
    def logout(self, userId):
        self.clearStateForUser(userId)
        # tornado.ioloop.IOLoop.current().add_callback(self.updateState, userId=userId,
        #                                              newUser =0)
        return {'status': 'success', 'userId': userId}

    @tornado.gen.coroutine
    def clearStateForUser(self,userId):
        #
        # if userId in self.clientSockets:
        #     del self.clientSockets[userId]

        for i in range (len(self.clientSockets)):
            if userId == self.clientSockets[i][0]:
               self.clientSockets[i].remove()

        users = yield User.load(DATAVERSE, userId = userId)
        if users:
            user = users[0]
            user.lastLogoffTime = str(datetime.now())
            yield user.save()
        if userId in self.sessions:
            del self.sessions[userId]

        # Clear subscriptions from the subscription Table
        for channelName in self.userSubscriptionTable:
            for subId in list(self.userSubscriptionTable[channelName]):
                if userId in self.userSubscriptionTable[channelName][subId]:
                    del self.userSubscriptionTable[channelName][subId][userId]

                    # Delete Channel Subscription if no active user subscription for it
                if not self.userSubscriptionTable[channelName][subId]:
                    channelSub = self.channelSubscriptionTable[channelName][subId]
                    self.deleteChannelSubscription(channelName, channelSub.channelSubscriptionId)
                    del self.userSubscriptionTable[channelName][subId]
                    del self.channelSubscriptionTable[channelName][subId]


    @tornado.gen.coroutine
    def notifyBroker(self,channelName, channelExecutionTime, channelSubscriptionIds):
        if channelName not in self.count:
            self.count[channelName] = -1
        if channelName not in self.pullData:
            self.pullData[channelName] = 0
        if channelName not in self.pushData:
            self.pushData[channelName] = 0
        if channelName not in self.delayTime:
            self.delayTime[channelName] = []
        if channelName not in self.queryTime:
            self.queryTime[channelName] = []
        if channelName not in self.deliveryTime:
            self.deliveryTime[channelName] = []

        # if channelName not in self.processingTime:
        #     self.processingTime[channelName]  = {}
        #     self.delayTime[channelName] = []
        # self.processingTime[channelName][channelExecutionTime] = {}

        # Register a callback to retrieve results for this notification and notify all users
        tornado.ioloop.IOLoop.current().add_callback(self.retrieveResults_andPush,
                                                     channelName, channelExecutionTime, channelSubscriptionIds)
        return {'status': 'success'}

    @tornado.gen.coroutine
    def retrieveResults_andPush(self,  channelName, channelExecutionTime, channelSubscriptionIds):

        # self.processingTime[channelName][channelExecutionTime]['pullData'] = 0
        # self.processingTime[channelName][channelExecutionTime]['pushData'] = 0
        pull_result_size=0
        push_result_size = 0
        query_time = 0
        startTime = datetime.now()
        # self.processingTime[channelName][channelExecutionTime]['startTime'] = datetime.now()

        # max_size = 0
        # maxSubId = None
        # subscriptionId = None

        # Retrieve the latest delivery times for the subscriptions in channelSubscriptionIds
        for channelSubscriptionId in channelSubscriptionIds:
            try:
                subscriptionId = self.subscriptionMapping[channelSubscriptionId]
            except KeyError as kerr:
                log.debug('No channel subscription %s' % (channelSubscriptionId))
                pass
            # Check if whether there is any subscription for this channel and any of them are active
            atleastOneActive = False
            try:
                atleastOneActive = any(userId in self.sessions
                                       for userId in self.userSubscriptionTable[channelName][subscriptionId])
            except KeyError as kerr:
                log.debug('No user subscribed to this channel %s--%s' % (channelName, subscriptionId))
                pass

            # Skip if no user has an active session
            if not atleastOneActive:
                log.debug('No user has an active session on %s--%s' % (channelName, subscriptionId))
                continue



            query = 'SELECT result FROM {0}Results ' \
                    'WHERE subscriptionId = uuid(\"{1}\") ' \
                    'and channelExecutionTime = datetime(\"{2}\")'.format(channelName,
                                                    channelSubscriptionId,
                                                    channelExecutionTime)

            log.debug(query)

            query_startTime = datetime.now()

            status, response = yield self.asterix.executeQuery(DATAVERSE, query)

            query_endTime = datetime.now()

            query_time += (query_endTime - query_startTime).total_seconds() * 1000

            log.debug(response)

            if status == 200 and response:
                results = json.loads(response)

                # size in bytes
                pull_result_size += sys.getsizeof(results) #len(results) #size in bytes
                push_result_size += sys.getsizeof(results) * len(self.userSubscriptionTable[channelName][subscriptionId]) #len(results) * len(self.userSubscriptionTable[channelName][subscriptionId])
                # print(subscriptionId, self.userSubscriptionTable[channelName][subscriptionId])
                # print('result_size: ', len(results))
                if results and len(results) > 0:
                    # tornado.ioloop.IOLoop.current().add_callback(self.pushAllUsers,
                    #                                              channelName=channelName,subscriptionId = subscriptionId, channelExecutionTime = channelExecutionTime, results = results)

                    delivery_startTime = datetime.now()

                    yield self.pushAllUsers(channelName, subscriptionId, channelExecutionTime, results)

                    delivery_endTime = datetime.now()

                    deliveryTime = (delivery_endTime - delivery_startTime).total_seconds() * 1000

        endTime = datetime.now()
        self.pullData[channelName] = pull_result_size
        self.pushData[channelName] = push_result_size
        self.queryTime[channelName] = query_time
        self.deliveryTime[channelName] = deliveryTime

        # self.processingTime[channelName][channelExecutionTime]['endTime'] = datetime.now()

        # self.processingTime[channelName][channelExecutionTime]['pullData'] +=pull_result_size
        # self.processingTime[channelName][channelExecutionTime]['pushData'] +=push_result_size
        # print('Time 1: ', endTime - startTime)
        # print('Time 2: ', self.processingTime[channelName][channelExecutionTime]['endTime'] - self.processingTime[channelName][channelExecutionTime]['startTime'])
        # if max_size < len(results):
                #     max_size = len(results)
                #     maxSubId = subscriptionId

        if self.count[channelName] < SIZE:
            self.count[channelName] += 1
        elif self.count[channelName] == SIZE:
            for i in range(SIZE):
                self.delayTime[channelName][i] = self.delayTime[channelName][i + 1]

        if (len(self.delayTime[channelName]) < (SIZE +1)):
            self.delayTime[channelName].append((endTime - startTime).total_seconds() * 1000)
            # self.delayTime[channelName].append((self.processingTime[channelName][channelExecutionTime]['endTime'] -
            #                                                               self.processingTime[channelName][
            #                                                                   channelExecutionTime]['startTime']).total_seconds() * 1000  )
        else: self.delayTime[channelName][SIZE] = (endTime - startTime).total_seconds() * 1000
        # else: self.delayTime[channelName][SIZE] = (self.processingTime[channelName][channelExecutionTime]['endTime'] -
        #                                                                   self.processingTime[channelName][
        #                                                                       channelExecutionTime]['startTime']).total_seconds() * 1000

        # print('Original Array for Channel ', channelName, '\n')
        # print(self.delayTime[channelName])
        MovingAverage = self.MovingAverage(self.delayTime[channelName], 5)

        num_FE_subs = 0

        for subId in list(self.userSubscriptionTable[channelName]):
            num_FE_subs += len(self.userSubscriptionTable[channelName][subId])

        print('Broker Delay Time per Execution Time for Channel : ', channelName, '\n')

        print('Total Delay: ',self.delayTime[channelName], '\n' )
        print('Query Delay: ', self.queryTime[channelName], '\n')
        print('Delivery Delay: ', self.deliveryTime[channelName], '\n')

        # print('Moving Average for Channel ', channelName, '\n')
        # print (MovingAverage)
        # print('\n Average: ', np.average(MovingAverage), '\n')
        print('Pull Data Amount in bytes : ',self.pullData[channelName], '\n')
        print('Push Data Amount in bytes : ', self.pushData[channelName], '\n')
        print('Number of BE Subs: ', len(self.channelSubscriptionTable[channelName]))
        print('Number of Users: ', len(self.sessions) )
        print('Number of FE Subs: ', num_FE_subs)
        # if (self.delayTime[channelName][channelExecutionTime] > CHANNEL_FREQUENCY[channelName]):
        #     print('Overload Detected!')

        # self.text_file.write(str(self.delayTime[channelName]))
        # self.text_file.write('\n')

        # tornado.ioloop.IOLoop.current().add_callback(self.updateState, delayTime=self.delayTime)

        #overload detection
        # dynamicLB = DynamicLoadBalancer()
        # dynamicLB.run()

        # del self.processingTime[channelName][channelExecutionTime]

    @tornado.gen.coroutine
    def pushAllUsers(self, channelName, subscriptionId, channelExecutionTime, results):
        for userId in self.userSubscriptionTable[channelName][subscriptionId]:
            # sub = self.userSubscriptionTable[channelName][subscriptionId][userId]
            # tornado.ioloop.IOLoop.current().add_callback(self.pushToUser, channelName= channelName, userId= userId, results = results )
            yield self.pushToUser(channelName, userId,results)

    @tornado.gen.coroutine
    def pushToUser(self, channelName, userId, results):
        if userId not in self.sessions:
            log.error('User %s is not logged in to receive results' % userId)
        # print('message: ', results)
        if userId in self.clientSockets.keys():
            clientSocket = self.clientSockets.get(userId)
            yield clientSocket.write_message(json.dumps(results))

    def MovingAverage(self, values,window):
        # weights = np.exp(np.linspace(-1, 0, window))
        # weights /= weights.sum()
        if len(values)==0:
            return
        if (len(values) < window):
            weights = np.repeat(1.0, len(values))/len(values)
        else:
            weights = np.repeat(1.0, window) / window

        a = np.convolve(values, weights, 'valid')
        return a