# from IPtoLatLong import IPtoLatLong
import os
import sys
# import geojsonio
# import geojson
# from geojson import Point
from StaticLoadBalancer import RandomLoadBalancer,StaticLoadBalancer # NearestLoadBalancer
from StateManagement import StateManager

import tornado.httpclient
import tornado.ioloop
import tornado.web
import tornado.websocket
import simplejson as json
from brokerobjects import *
from tornado.httpserver import HTTPServer

DATAVERSE = 'pubsubtest'

class BaseHandler(tornado.web.RequestHandler):
    def post(self):
        pass

class MainHandler(BaseHandler):
    def post(self):
        pass

class GetBrokerHandler(BaseHandler):
    @tornado.gen.coroutine
    def post(self):
        # log.info(str(self.request.body, encoding='utf-8'))
        post_data = json.loads(str(self.request.body, encoding='utf-8'))
        print(self.request.headers)
        log.debug(post_data)
        try:
            userId = post_data['userId']
            clientIP = self.request.headers.get('ClientIP')
        except KeyError as e:
            return {'status': 'failed', 'error': 'Bad formatted request missing field ' + str(e)}

        # print('clientIP = ', clientIP)
        # location = IPtoLatLong.getLatLong(IPtoLatLong(), ipAddress = clientIP)
        location = (0,0)
        # print('location: ', location)
        brokerName, brokerIP, brokerPort = RandomLoadBalancer().getNext()
        yield StateManager.getInstance().newUser(userId, location, brokerName, brokerIP, brokerPort)
        # client_Point = geojson.Point(location)
        # geojsonio.display(client_Point)
        #user login
        response = {'brokerServer': brokerIP, 'brokerPort': brokerPort}
        # response = {'brokerServer': 'localhost', 'brokerPort': 8989}
        # print('return broker: ', response)
        # print('Client State _: ', StateManager.getInstance().clientState)

        self.write(json.dumps(response))
        self.flush()
        self.finish()

class RegisterBrokerHandler(BaseHandler):
    def post(self):
        log.info(str(self.request.body, encoding='utf-8'))
        post_data = json.loads(str(self.request.body, encoding='utf-8'))
        log.debug(post_data)

        try:
            brokerName = post_data['brokerName']
            brokerIP = post_data['brokerIP']
            brokerPort = post_data['brokerPort']
            print ("Broker registered Successful!.")
        except:
            print ("Broker registered Failed!")

        # location = IPtoLatLong.getLatLong(IPtoLatLong(), ipAddress = brokerIP)
        location = (0,0)
        StateManager.getInstance().newBroker(brokerName, brokerIP, brokerPort , location)

        # print('Broker State: ',  StateManager.getInstance().brokerState)

class GetBrokerUpdateHandler(BaseHandler):
    @tornado.gen.coroutine
    def post(self):

        delayTime = {}
        # log.info(str(self.request.body, encoding='utf-8'))
        post_data = json.loads(str(self.request.body, encoding='utf-8'))
        log.debug(post_data)
        try:
            brokerName = post_data['brokerName']
            brokerIP = post_data['brokerIP']
            brokerPort = post_data['brokerPort']
            userId = post_data['userId']
            subId = post_data['subId']
            newUser = post_data['newUser']
            newSub =  post_data['newSub']
            delayTime = post_data['delayTime']

        except:
            return "Broker update failed!"

        # user logout
        if userId and (newUser == 0):
            StateManager.getInstance().delUser(brokerName,userId)

        #new subscription
        if userId and (newSub == 1):
            StateManager.getInstance().newSub(brokerName,  userId,subId)

        # unsubscribe
            StateManager.getInstance().delSub(brokerName, userId, subId)

        #update delayTime
        if (delayTime):
            StateManager.getInstance().updateDelayTime(brokerName,  delayTime)

        # print('Broker State: ', StateManager.getInstance().brokerState)
        # print('Client State: ', StateManager.getInstance().clientState)


class RegisterUserHandler(BaseHandler):
    @tornado.gen.coroutine
    def post(self):
        # log.info(str(self.request.body, encoding='utf-8'))
        post_data = json.loads(str(self.request.body, encoding='utf-8'))
        log.debug(post_data)
        try:
            userName = post_data['userName']
            userId = post_data['userId']
            email = post_data['email']
            password = post_data['password']
            users = yield User.load(DATAVERSE, userName)
            if users and len(users) > 0:
                response = {
                    'status': 'failed',
                    'error': 'This user already exists!',
                    'userId': userId}
            else:
                password = hashlib.sha224(password.encode()).hexdigest()
                user = User(DATAVERSE, userId, userId, userName, password, email)
                yield user.save()
                response ={'status': 'success', 'userId': userId}

        except KeyError as e:
            log.info('Parse error for ' + str(e) + ' in ' + str(post_data))
            log.info(e.with_traceback())
            response = {'status': 'failed', 'error': 'Bad formatted request missing field ' + str(e)}

        self.write(json.dumps(response))
        self.flush()
        self.finish()

if __name__ == '__main__':

    application = tornado.web.Application([
         (r'/', MainHandler),
         (r'/registerbroker', RegisterBrokerHandler),
         (r'/registeruser', RegisterUserHandler),
         (r'/getBroker', GetBrokerHandler),
         (r'/brokerUpdate', GetBrokerUpdateHandler)
    ])

    server = HTTPServer(application, xheaders = True)
    server.listen(5000)
    tornado.ioloop.IOLoop.current().start()
