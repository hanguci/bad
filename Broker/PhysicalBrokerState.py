import json
import psutil



class PhysicalLoadInfo(object):

    def __init__(self, jsonData=None):

        if jsonData is not None:
            self.__dict__ = json.loads(jsonData)
        else:
            self.cpuUsage = 0
            self.memoryUsage = 0

    def getPhysicalLoad(self):

        self.cpuUsage = psutil.cpu_percent()
        self.memoryUsage = psutil.virtual_memory().percent
