import os
import sys
import tornado.httpclient
import tornado.ioloop
import tornado.web
import tornado.websocket
import simplejson as json
from broker import *

mutex = Lock()
condition_variable = False

DATAVERSE = 'pubsubtest'

class BaseHandler(tornado.web.RequestHandler):
    def initialize(self, broker):
        self.broker = broker

    def post(self):
        pass


class MainHandler(BaseHandler):
    def initialize(self, broker):
        self.broker = broker
    def get(self):
        self.render("static/index.html")

    def post(self):
        pass

class RegisterApplicationHandler(BaseHandler):
    def initialize(self, broker):
        self.broker = broker

    def post(self):
        response = None
        if len(self.get_body_arguments('fromForm')) > 0:
            try:
                dropExisting = self.get_body_arguments('dropExisting')
                setupAQL = self.get_body_arguments('setupAQL')

                if len(dropExisting) > 0 and len(setupAQL) > 0:
                    response =  self.broker.registerApplication(DATAVERSE, DATAVERSE, '', '', '', dropExisting=1, setupAQL=setupAQL[0])
                elif len(dropExisting) > 0:
                    response =  self.broker.registerApplication(DATAVERSE, DATAVERSE, '', '', '', dropExisting=1)
                elif len(setupAQL) > 0:
                    response =self.broker.registerApplication(DATAVERSE, DATAVERSE, '', '', '', setupAQL=setupAQL[0])
                else:
                    response =self.broker.registerApplication(DATAVERSE, DATAVERSE, '', '', '')

            except tornado.web.MissingArgumentError as e:
                response = {'status': 'failed', 'error': 'Bad formatted request missing field ' + str(e)}

            except Exception as e:
                response = {'status': 'failed', 'error': str(e)}
        else:
            self.write_error("Empty Application!")
        self.write(json.dumps(str(response)))
        self.flush()
        self.finish()


class RegisterUserHandler(BaseHandler):
    def initialize(self, broker):
        self.broker = broker

    @tornado.gen.coroutine
    def post(self):
        log.info(str(self.request.body, encoding='utf-8'))
        post_data = json.loads(str(self.request.body, encoding='utf-8'))
        log.debug(post_data)

        try:
            userName = post_data['userName']
            email = post_data['email'] if 'email' in post_data else 'none'
            password = post_data['password']
            response = yield self.broker.register( userName, password, email)
        except KeyError as e:
            log.info('Parse error for ' + str(e) + ' in ' + str(post_data))
            log.info(e.with_traceback())
            response = {'status': 'failed', 'error': 'Bad formatted request missing field ' + str(e)}

        self.write(json.dumps(response))
        self.flush()
        self.finish()


class LoginHandler(BaseHandler):
    def initialize(self, broker):
        self.broker = broker

    @tornado.gen.coroutine
    def post(self):
        log.info(str(self.request.body, encoding='utf-8'))
        post_data = json.loads(str(self.request.body, encoding='utf-8'))
        log.debug(post_data)
        try:
            userName = post_data['userName']
            password = post_data['password']
            response = yield self.broker.login( userName, password)
        except KeyError as e:
            response = {'status': 'failed', 'error': 'Bad formatted request missing field ' + str(e)}
        log.debug(response)
        self.write(json.dumps(response))
        self.flush()
        self.finish()


class LogoutHandler(BaseHandler):
    def initialize(self, broker):
        self.broker = broker

    @tornado.gen.coroutine
    def post(self):
        log.info(str(self.request.body, encoding='utf-8'))
        post_data = json.loads(str(self.request.body, encoding='utf-8'))
        log.debug(post_data)
        try:
            userId = post_data['userId']
            response = yield self.broker.logout( userId)
        except KeyError as e:
            response = {'status': 'failed', 'error': 'Bad formatted request missing field ' + str(e)}
            print("logout response ", response)

        self.write(json.dumps(response))
        self.flush()
        self.finish()

class resultsWebSocketHandler(tornado.websocket.WebSocketHandler):
    def initialize(self, broker):
        self.broker = broker

    def open(self):
        self.broker.connections.add(self)
        # print("connected")

    def on_message(self, message):
        # for client in self.broker.connections:
        #     client.write_message(message)

        post_data = json.loads(message)
        # print('Message incoming:', message)
        userId = post_data['userId']
        self.broker.clientSockets[userId] = self

    def on_close(self):
        pass
        # self.broker.connections.remove(self)
        self.broker.clientSockets = {k: v for k, v in self.broker.clientSockets.items() if v != self}
        log.info("WebSocket closed")

class SubscriptionHandler(BaseHandler):
    def initialize(self, broker):
        self.broker = broker

    @tornado.gen.coroutine
    def post(self):
        # log.info(str(self.request.body, encoding='utf-8'))
        post_data = json.loads(str(self.request.body, encoding='utf-8'))
        log.debug(post_data)

        try:
            userId = post_data['userId']
            channelName = post_data['channelName']
            parameters = post_data['parameters']
            response = yield self.broker.subscribe(userId, channelName, parameters)

        except KeyError as e:
            log.error(str(e))
            response = {'status': 'failed', 'error': 'Bad formatted request'}

        self.write(json.dumps(response))
        self.flush()
        self.finish()

class UnsubscriptionHandler(BaseHandler):
    def initialize(self, broker):
        self.broker = broker

    @tornado.gen.coroutine
    def post(self):
        # log.info(str(self.request.body, encoding='utf-8'))
        post_data = json.loads(str(self.request.body, encoding='utf-8'))
        log.debug(post_data)
        try:
            userId = post_data['userId']
            channelName = post_data['channelName']
            parameters = post_data['parameters']
            response = yield self.broker.unsubscribe( userId, channelName, parameters)
        except KeyError as e:
            response = {'status': 'failed', 'error': 'Bad formatted request'}

        self.write(json.dumps(response))
        self.flush()
        self.finish()

class GetResultsHandler(BaseHandler):
    def initialize(self, broker):
        self.broker = broker

    @tornado.gen.coroutine
    def post(self):
        log.info(str(self.request.body, encoding='utf-8'))
        post_data = json.loads(str(self.request.body, encoding='utf-8'))
        log.debug(post_data)

        try:
            userId = post_data['userId']
            userSubscriptionId = post_data['userSubscriptionId']
            channelExecutionTime = post_data['channelExecutionTime'] if 'channelExecutionTime' in post_data else None
            resultSize = post_data['resultSize'] if 'resultSize' in post_data else None
            response = yield self.broker.getResults(userId, userSubscriptionId, channelExecutionTime, resultSize)
        except KeyError as e:
            response = {'status': 'failed', 'error': 'Bad formatted request missing field ' + str(e)}

        log.info(json.dumps(response))
        self.write(json.dumps(response))
        self.flush()
        self.finish()


class NotifyBrokerHandler(BaseHandler):
    def initialize(self, broker):
        self.broker = broker

    @tornado.gen.coroutine
    def post(self):
        # log.info('Broker received notifybroker')
        # log.info(str(self.request.body, encoding='utf-8'))
        global condition_variable

        post_data = json.loads(self.request.body)
        log.debug(post_data)
        channelName = post_data['channelName']
        channelExecutionTime = post_data['channelExecutionTime']
        subscriptionIds = post_data['subscriptionIds']
        response = yield self.broker.notifyBroker(channelName, channelExecutionTime, subscriptionIds)
        mutex.acquire()
        try:
            condition_variable = True
        finally:
            mutex.release()

        self.write(json.dumps(response))
        self.flush()
        self.finish()


class ListChannelsHandler(BaseHandler):
    def initialize(self, broker):
        self.broker = broker

    @tornado.gen.coroutine
    def post(self):
        log.info('Broker received listchannels')
        log.info(str(self.request.body, encoding='utf-8'))
        post_data = json.loads(self.request.body)
        log.debug(post_data)
        userId = post_data['userId']
        response = yield self.broker.listchannels( userId)

        self.write(json.dumps(response))
        self.flush()
        self.finish()


class ListSubscriptionsHandler(BaseHandler):
    def initialize(self, broker):
        self.broker = broker

    @tornado.gen.coroutine
    def post(self):
        log.info('Broker received listsubscriptions')
        log.info(str(self.request.body, encoding='utf-8'))
        post_data = json.loads(self.request.body)
        log.debug(post_data)
        userId = post_data['userId']

        response = yield self.broker.listsubscriptions(userId)

        self.write(json.dumps(response, for_json=True))
        self.flush()
        self.finish()


def start_broker(brokerName, brokerPort, bool):

    broker = Broker(brokerName, brokerPort)

    if bool:
        textfile = open("setupAQL1", "r")
        setupAQL = textfile.read()
        textfile.close()

        # file = open("publishers", "r")
        # dataPublished = file.read()
        # print(dataPublished)
        # file.close()
        broker.registerApplication(DATAVERSE, DATAVERSE, 'admin', 'password', 'admin@admin', dropExisting=1,
                                   setupAQL=setupAQL)

        # broker.populateData(DATAVERSE, dataPublished)
        # asterix = AsterixQueryManager.getInstance()

        # status, response = yield asterix.executeSQLPP(DATAVERSE, dataPublished)
        #
        # if status == 200:
        #     log.info('Publishing Data Success!')

    settings = {
        "static_path": os.path.join(os.path.dirname(__file__), "static")
    }

    application = tornado.web.Application([
         (r'/', MainHandler, dict(broker=broker)),
         (r'/registerapplication', RegisterApplicationHandler, dict(broker=broker)),
         (r'/registeruser', RegisterUserHandler, dict(broker=broker)),
         (r'/login', LoginHandler, dict(broker=broker)),
         (r'/logout', LogoutHandler, dict(broker=broker)),
         (r'/subscribe', SubscriptionHandler, dict(broker=broker)),
         (r'/unsubscribe', UnsubscriptionHandler, dict(broker=broker)),
         (r'/getresults', GetResultsHandler, dict(broker=broker)),
         (r'/notifybroker', NotifyBrokerHandler, dict(broker=broker)),
         (r'/listchannels', ListChannelsHandler, dict(broker=broker)),
         (r'/listsubscriptions', ListSubscriptionsHandler, dict(broker=broker)),
         (r'/websocketlistener', resultsWebSocketHandler, dict(broker=broker))

    ], **settings)

    application.listen(broker.brokerPort)
    print('Broker {0} Listening on {1}...'.format(broker.brokerIPAddr, broker.brokerPort))
    tornado.ioloop.IOLoop.current().start()

if __name__ == '__main__':

    count = 0

    # if len(sys.argv) < 2:
    #     start_broker()
    # else:
    #     start_broker(sys.argv[1])
    try:
        # start_broker(sys.argv[1], sys.argv[2])
        start_broker('Broker', 8989, True)

    except Exception as e:
        print('Start Broker Failed!')
        print(e)
        exit(0)