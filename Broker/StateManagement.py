
from brokerobjects import *

DATAVERSE = 'pubsubtest'

class StateManager(object):
    instance = None

    @classmethod
    def getInstance(cls):
        if cls.instance is None:
            cls.instance = StateManager()
        return cls.instance

    def __init__(self):
        self.brokerState = {}
        self.clientState = {}
        self.brokerList = []
        self.roundRobinIndex = -1

    @tornado.gen.coroutine
    def newUser(self, userId, location, brokerName, brokerIP, brokerPort):

        subscriptionList = []
        userSubscriptions = yield UserSubscription.load(dataverseName=DATAVERSE, userId=userId)
        if userSubscriptions:
            for i in range(len(userSubscriptions)):
                subscriptionList.append(userSubscriptions[i].subscriptionId)
        if userId not in self.clientState:
            self.clientState[userId] = {}
        self.clientState[userId]['location'] = location
        self.clientState[userId]['broker'] = (brokerName, brokerIP, brokerPort)
        self.clientState[userId]['sub'] = subscriptionList
        if brokerName in self.brokerState:
            if userId not in self.brokerState[brokerName]:
                self.brokerState[brokerName][userId] = []
        self.brokerState[brokerName][userId] = subscriptionList


    def newBroker(self, brokerName, brokerIP, brokerPort,location):
        if brokerName not in self.brokerState:
            self.brokerState[brokerName] = {}
        if (brokerName, brokerIP, brokerPort) not in self.brokerList:
            self.brokerList.append((brokerName, brokerIP, brokerPort))
            print(self.brokerList)

        self.brokerState[brokerName]['location'] = location
        self.brokerState[brokerName]['IP'] = brokerIP
        self.brokerState[brokerName]['port'] = brokerPort
        self.brokerState[brokerName]['delayTime'] = {}

    def delUser(self, brokerName, userId):
        if brokerName in self.brokerState:
            if userId in self.brokerState[brokerName]:
                del self.brokerState[brokerName][userId]
        if userId in self.clientState:
            del self.clientState[userId]

    def newSub(self, brokerName, userId, subId):
        if brokerName in self.brokerState:
            if userId in self.brokerState[brokerName]:
                self.brokerState[brokerName][userId].append(subId)
        if userId in self.clientState:
            self.clientState[userId]['sub'].append(subId)

    def delSub(self,  brokerName, userId, subId):
        if brokerName in self.brokerState:
            if userId in self.brokerState[brokerName]:
                if subId in self.brokerState[brokerName][userId]:
                    self.brokerState[brokerName][userId].remove(subId)
        if userId in self.clientState:
            if subId in self.clientState[userId]:
                self.clientState[userId].remove(subId)

    def updateDelayTime(self, brokerName, delayTime):
        if (delayTime != {}):
            if brokerName in self.brokerState:
                self.brokerState[brokerName]['delayTime']= delayTime
        else:
            if brokerName in self.brokerState:
                self.brokerState[brokerName]['delayTime'] = None
